package com.quartz.controller.common;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

@Controller
public class CommonAjaxController {

	protected final Logger logger = Logger.getLogger(getClass());

	/**
	 * 异步请求 status返回状�?? returnMessage 返回携带信息
	 * 
	 * @param response
	 * @param status
	 * @param errorMessage
	 */
	protected void returnAjaxRequestMessage(HttpServletResponse response,
			String status, String returnMessage) {
		try {
			response.setContentType("application/json;charset=UTF-8");
			response.setHeader("Access-Control-Allow-Origin", "*");
			Map<String, String> map3 = new HashMap<String, String>();
			map3.put("status", status);
			map3.put("message", returnMessage);
			response.getWriter()
					.println(JSONObject.fromObject(map3).toString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch (IOException e) {
			logger.error(e);
			return;
		}
	}


	/**
	 * 异步请求返回json结果Object形式
	 * 
	 * @param response
	 * @param status
	 * @param returnMessage
	 */
	protected void returnAjaxObjectJson(HttpServletResponse response, Object obj) {
		try {
			response.setContentType("application/json;charset=UTF-8");
			response.setHeader("Access-Control-Allow-Origin", "*");
			JsonConfig jsonConfig = new JsonConfig();
			jsonConfig.registerJsonValueProcessor(Date.class,
					new JsonDateValueProcessor());
			response.getWriter().println(
					JSONObject.fromObject(obj, jsonConfig).toString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch (IOException e) {
			logger.error(e);
			return;
		}
	}

	/**
	 * 异步请求返回json结果
	 * 
	 * @param response
	 * @param status
	 * @param returnMessage
	 */
	protected void returnAjaxArrayJson(HttpServletResponse response, Object obj) {
		try {
			response.setContentType("application/json;charset=UTF-8");
			response.setHeader("Access-Control-Allow-Origin", "*");
			JsonConfig jsonConfig = new JsonConfig();
			jsonConfig.registerJsonValueProcessor(Date.class,
					new JsonDateValueProcessor());
			response.getWriter().println(
					JSONArray.fromObject(obj, jsonConfig).toString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch (IOException e) {
			logger.error(e);
			return;
		}
	}
}
