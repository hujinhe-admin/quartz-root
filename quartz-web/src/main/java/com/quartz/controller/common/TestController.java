package com.quartz.controller.common;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.quartz.service.UserService;

@RestController
@RequestMapping("/")
public class TestController extends CommonAjaxController {
	@Autowired
	UserService userService;
	private Logger logger = Logger.getLogger(TestController.class);
	@RequestMapping(value = "/test.do", method = RequestMethod.GET)
	public void createOrder(HttpServletRequest request, HttpServletResponse response, String md5, String data)
			throws Exception {
		Map<String, Object> map=new HashMap<String, Object>();
		Map<String, Object> map2=new HashMap<String, Object>();
		map2.put("code", 0);
		map2.put("message", "服务器启动成功！");
		map.put("response", map2);
		userService.say();
		this.returnAjaxObjectJson(response, map);
	}
}
