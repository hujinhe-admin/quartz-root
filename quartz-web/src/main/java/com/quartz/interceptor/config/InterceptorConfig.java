package com.quartz.interceptor.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.quartz.interceptor.LoginInterceptor;

@Configuration
public class InterceptorConfig extends WebMvcConfigurerAdapter {
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 注册自定义拦截器，添加拦截路径和排除拦截路径
		// registry.addInterceptor(new LoginInterceptor())
		// .addPathPatterns("/**/*.do")
		// .excludePathPatterns("/member/member_login_view.do")
		// .excludePathPatterns("/member/member_login.do")
		// .excludePathPatterns("/member/member_regist_view.do")
		// .excludePathPatterns("/member/member_regist.do")
		// .excludePathPatterns("/doctor/doctor_login_view.do")
		// .excludePathPatterns("/doctor/doctor_login.do")
		// .excludePathPatterns("/index.do")
		// .excludePathPatterns("/article_list.do")
		// .excludePathPatterns("/article_detail.do");
	}
}
