package com.quartz.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 * 
 * @date 2010-11-24
 * @since JDK 1.5
 * @author liaijun
 * @version 1.0
 */
public class StringUtil extends org.apache.commons.lang.StringUtils {

    /**
     * 存放国标一级汉字不同读音的起始区位码
     */
    static final int[] secPosValueList = { 1601, 1637, 1833, 2078, 2274, 2302, 2433, 2594, 2787, 3106, 3212, 3472,
            3635, 3722, 3730, 3858, 4027, 4086, 4390, 4558, 4684, 4925, 5249, 5600 };

    /**
     * 存放国标一级汉字不同读音的起始区位码对应读音
     */
    static final char[] firstLetter = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
            'r', 's', 't', 'w', 'x', 'y', 'z' };

    /**
     * 去除字符串中的空格、回车、换行符、制表符
     * 
     * @param str
     *            源字符串
     * @return 处理后的字符串
     */
    public static String replaceBlank(String str) {
        Pattern p = Pattern.compile("\\s*|\t|\r|\n");
        Matcher m = p.matcher(str);
        String after = m.replaceAll("");
        return after;
    }
    
    //生成随机6位数字
    public static String getSix(){  
        Random rad=new Random();  
        String result  = rad.nextInt(1000000) +"";  
        if(result.length()!=6){  
            return getSix();  
        }  
        return result;  
    }
    //生成随机4位数字
    public static String getFour(){  
        Random rad=new Random();  
        String result  = rad.nextInt(10000) +"";  
        if(result.length()!=4){  
            return getFour();  
        }  
        return result;  
    } 

    /**
     * 得到c在s中的出现的索引列表
     * 
     * @param s
     *            原字符串
     * @param c
     *            子字符串
     * @return c在s中出现的索引列表
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List getIndexList(String s, String c) {
        int x = s.indexOf(c);
        int replaceLenght = 0;
        List list = new ArrayList();
        while (x != -1) {
            list.add(x + "");
            s = s.replaceFirst(c, "");
            replaceLenght = replaceLenght + c.length();
            x = s.indexOf(c);
            if (x != -1) {
                x = s.indexOf(c) + replaceLenght;
            }
        }
        return list;
    }
    
    /** 
     * 手机号验证 
     *  
     * @param  str 
     * @return 验证通过返回true 
     */  
    public static boolean isMobile(String str) {   
        Pattern p = null;  
        Matcher m = null;  
        boolean b = false;   
        p = Pattern.compile("^[1][3,4,5,8][0-9]{9}$"); // 验证手机号  
        m = p.matcher(str);  
        b = m.matches();   
        return b;  
    }  

    /**
     * 判断是否为数字（包括小数和整数）
     * 
     * @param str
     *            要判断的字符串
     * @return true/flase（是/否）
     */
    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("-?[0-9]*.?[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }

    /**
     * 获取一个字符串的拼音码
     * 
     * @param oriStr
     *            要操作的字符串
     * @return 拼音码
     */
    public static String getFirstLetter(String oriStr) {
        String str = oriStr.toLowerCase();
        StringBuffer buffer = new StringBuffer();
        char ch;
        char[] temp;
        for (int i = 0; i < str.length(); i++) {
            // 依次处理str中每个字符
            ch = str.charAt(i);
            temp = new char[] { ch };
            byte[] uniCode = new String(temp).getBytes();
            if (uniCode[0] < 128 && uniCode[0] > 0) {
                // 非汉字
                buffer.append(temp);
            }
            else {
                buffer.append(convert(uniCode));
            }
        }
        return buffer.toString();
    }

    /**
     * 获取一个汉字的拼音首字母
     * 
     * @param bytes
     *            要操作的字符串
     * @return 拼音首字母
     */
    public static char convert(byte[] bytes) {
        char result = '-';
        int secPosValue = 0;
        int i;
        for (i = 0; i < bytes.length; i++) {
            bytes[i] -= 160;
        }
        secPosValue = bytes[0] * 100 + bytes[1];
        for (i = 0; i < 23; i++) {
            if (secPosValue >= secPosValueList[i] && secPosValue < secPosValueList[i + 1]) {
                result = firstLetter[i];
                break;
            }
        }
        return result;
    }

    /**
     * 比较两个字符串的大小,按拼音顺序
     * 
     * @param str1
     *            要操作的字符串
     * @param str2
     *            要操作的字符串
     * @return -1:表示str1<str2 ; 1:表示str1>str2 ;0:表示str1=str2
     */
    public static int compareString(String str1, String str2) {
        int m = str1.length();
        int n = str2.length();
        for (int i = 0; i < m; i++) {
            if (i < n) {
                if (str1.charAt(i) > str2.charAt(i)) {
                    return 1;
                }
                else if (str1.charAt(i) == str2.charAt(i)) {
                    if (m == n && i + 1 == m) {
                        return 0;
                    }
                    else {
                        continue;
                    }
                }
                else {
                    return -1;
                }
            }
            else {
                return 1;
            }
        }
        return -1;
    }

    /**
     * 替换字符串
     * @param resource 要操作的字符串
     * @param target 要替换的目标子串
     * @param result 用来替换目标子串的字符串
     * @return 替换后的字符串
     */
    public static String replaceAllStr(String resource, String target, String result) {
        resource = resource.replaceAll(target, result);
        return resource;
    }

    /**
     * 将object 转为 string value并去空格
     * 若object为null返回空字串
     * @param value
     * @return 转换后的字符串
     */
    public static String getString(Object value) {
        if (value == null) {
            return "";
        }
        return String.valueOf(value).trim();
    }

    /**
     * 将字符串转为整形值
     * @param value
     * @return 转换后的int数字
     */
    public static int parseStringToInt(String value) {
        try {
            if (value == null || value.trim().equals("")) {
                return 0;
            }
            return Integer.parseInt(value);
        }
        catch (Exception ex) {
            return 0;
        }
    }
    
    /**
     * 根据分割符','，将输入字符串转换为String数组
     * 
     * @param value
     * @return
     */

    public static String arrayToCSV(String[] value) {
        return arrayToDelimited(value, ",", true, true);
    }
    
    /**
     * 根据分割符，将输入字符串转换为String数组,以','分割
     * 
     * @param value
     * @return
     */

    public static String arrayToDelimited(Object[] value) {
        return arrayToDelimited(value, ",");
    }
    
    /**
     * 根据分割符，将输入字符串转换为String数组
     * 
     * @param value
     * @param delimiter
     * @return
     */

    public static String arrayToDelimited(Object[] value, String delimiter) {
        return arrayToDelimited(value, delimiter, false, false, false);
    }

    /**
     * 根据分割符，将输入字符串转换为String数组
     * 
     * @param value
     * @param delimiter
     * @return
     */

    public static String arrayToDelimited(String[] value, String delimiter) {
        return arrayToDelimited(value, delimiter, true, true);
    }
    
    /**
     * 根据分割符，将输入字符串转换为String数组
     * 
     * @param value
     * @param delimiter
     * @param prepend
     * @param append
     * @return
     */
    public static String arrayToDelimited(String[] value, String delimiter,
            boolean prepend, boolean append) {
        return arrayToDelimited(value, delimiter, prepend, append, false);
    }
    
    /**
     * 根据分割符，将输入字符串转换为String数组
     * 
     * @param value
     * @param delimiter
     * @param prepend
     * @param append
     * @param eliminateDuplicates
     * @return
     */
    public static String arrayToDelimited(Object[] value, String delimiter,
            boolean prepend, boolean append, boolean eliminateDuplicates) {
        if (delimiter == null)
            delimiter = ",";
        String retVal = null;
        if (value != null) {
            StringBuffer buff = new StringBuffer();
            int length = value.length;
            if (length > 0) {
                if (prepend)
                    buff.append(delimiter);
                boolean isDuplicateValue = false;
                buff.append(delimiter); // Always make sure the buff starts with
                // a delimiter for duplicate checking
                for (int i = 0; i < length; i++) {
                    isDuplicateValue = (eliminateDuplicates ? (buff
                            .indexOf(delimiter + value[i] + delimiter) != -1)
                            : false);
                    if (!isDuplicateValue) {
                        buff.append(value[i]);
                        if (i < length - 1)
                            buff.append(delimiter);
                    }
                }
                buff.deleteCharAt(0); // remove the delimiter added for checking
                // duplicates
                // If the last value is a duplicate value, remove the delimiter
                // added to the end of the string
                if (isDuplicateValue) {
                    buff.deleteCharAt(buff.length() - 1);
                }
                if (append)
                    buff.append(delimiter);
            }
            retVal = buff.toString();
        }
        return retVal;
    }

	/** 
	 * 判断页面传来的参数是否为空
	 * @Title: isParamEmpty 
	 * @Description: TODO
	 * @param paramName
	 * @return
	*/ 
	
	public static boolean isParamEmpty(String paramName) {
		// TODO 自动生成方法存根
		if(paramName!=null && !paramName.equals("") && !paramName.equals("null"))
			return false;
		else 
			return true;
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * 通过反射将字符串转换为基本数据类型
	 * 
	 * @author ex_xupengfei
	 * @param str
	 * @param clz
	 * @return
	 * @throws Exception
	 */
	public static <T> T transferString2PrimitiveType(String str, Class<T> clz)
			throws Exception {
		T obj = null;
		try {
			if (Boolean.class == clz || boolean.class == clz) {
				obj = (T) Boolean.valueOf(str);
			} else if (Character.class == clz || char.class == clz) {
				obj = (T) (Character) str.toCharArray()[0];
			} else if (Short.class == clz || short.class == clz) {
				obj = (T) Short.valueOf(str);
			} else if (Integer.class == clz || int.class == clz) {
				obj = (T) Integer.valueOf(str);
			} else if (Byte.class == clz || byte.class == clz) {
				obj = (T) Byte.valueOf(str);
			} else if (Long.class == clz || long.class == clz) {
				obj = (T) Long.valueOf(str);
			} else if (Double.class == clz || double.class == clz) {
				obj = (T) Double.valueOf(str);
			} else if (Float.class == clz || float.class == clz) {
				obj = (T) Float.valueOf(str);
			}
		} catch (Exception e) {
			throw new Exception(str + "无法转换为" + clz.getSimpleName() + "类型");
		}
		return obj;
	}
	
	/**
	 * 获取指定前缀和后缀之间的内容，并返回一个内容列表
	 * @param str 操作的目标字符串
	 * @param c1 前缀字符
	 * @param c2 后缀字符
	 * @return 挖出的子串列表
	 */
	public static List<String> dugSubstrings(String str, char c1, char c2) {
		List<String> result = new ArrayList<String>();
		int index1 = -1;
		int index2 = -1;
		while ((index1 = str.indexOf(c1, index2 + 1)) != -1
				&& (index2 = str.indexOf(c2, index1 + 1)) != -1) {
			result.add(str.substring(index1 + 1, index2));
		}
		return result;
	}
	
	/**
	 * 用指定符号将字符串分隔，同时支持忽略指定前缀和后缀之间的分隔符号
	 * @param str 操作的目标字符串
	 * @param gap 分隔符
	 * @param exclude1 前缀
	 * @param exclude2 后缀
	 * @return 切分后的子串数组
	 */
	public static String[] splitStringByGap(String str, char gap,
			String exclude1, String exclude2) {
		List<String> strs = new ArrayList<String>();
		int j = -1;
		int i = j;
		j = str.indexOf(gap, i + 1);
		while (j != -1) {
			int t = 0;
			if (str.indexOf(exclude1) < j && (t = str.indexOf(exclude2, j)) > j) {
				j = str.indexOf(gap, t);
			}
			if (j != -1) {
				strs.add(str.substring(i + 1, j));
				i = j;
				j = str.indexOf(gap, i + 1);
			}
		}
		if (i != -1) {
			strs.add(str.substring(i + 1));
		}
		return strs.toArray(new String[1]);
	}

	/**
	 * 判断是否是中文
	 * @param str
	 * @return
	 */
	public static boolean isChinese(String str) {
		Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        if (m.find()) {
            return true;
        }
        return false;
	}
	
	/** 
	* @Description: 分割id字符串
	* @param str 源字符串
	* @param tag 分割标记
	* @return long型列表
	*/
	public static List<Long> splitIds(String str,String tag) {
		List<Long> list = new ArrayList<Long>();
		if(StringUtil.isNotBlank(str)) {
			if(!str.contains(",")) {
				list.add(Long.valueOf(str));
				return list;
			} else {
				String s[] = str.split(tag);
				for(int i = 0;i < s.length;i++) {
					list.add(Long.valueOf(s[i]));
				}
				return list;
			}
		}
		return null;
	}
	
	/**
	 * isNull(如果为""或则为null则返回null) 
	 * @Title: isNull 
	 * @Description: TODO 
	 * @param: @param str
	 * @param: @return
	 * @return: String 
	 * @throwsException:
	 * @author:lipei-lxy 
	 * @updatetime:2015年5月26日 下午11:31:15
	 */
	public static String isNull(String str){
		if(StringUtil.isBlank(str)){
			return null;
		}
		return str;
	}
	
	/**
	 * changNumToStrNum(小写的数字改为大写) 
	 * @Title: changNumToStrNum 
	 * @param: @param num
	 * @return: String 
	 * @throwsException:
	 * @author:xhaoxian
	 * @updatetime:2015年9月14日 下午11:31:15
	 */
	 public static String changNumToStrNum(String num){
	    	String strNum="";
	    	if("1".equals(num)){
	    		strNum="一";
	    	}else if("2".equals(num)){
	    		strNum="二";
	    	}else if("3".equals(num)){
	    		strNum="三";
	    	}else if("4".equals(num)){
	    		strNum="四";
	    	}else if("5".equals(num)){
	    		strNum="五";
	    	}else if("6".equals(num)){
	    		strNum="六";
	    	}else if("7".equals(num)){
	    		strNum="七";
	    	}else if("8".equals(num)){
	    		strNum="八";
	    	}else if("9".equals(num)){
	    		strNum="九";
	    	}
	    	return strNum;
	    }
	 
	 public static String getPhoneByStr(String phone) {
			if (StringUtil.isBlank(phone)) {
				return phone;
			}
			phone = phone.replaceAll(" ", "").replaceAll("　", "");
			Set<String> set = new HashSet<String>();
			set.add("+");
			set.add("0");
			set.add("1");
			set.add("2");
			set.add("3");
			set.add("4");
			set.add("5");
			set.add("6");
			set.add("7");
			set.add("8");
			set.add("9");
			String finalPhone = "";
			for (int i = 0; i < phone.length(); i++) {
				if (set.contains(String.valueOf(phone.charAt(i)))) {
					finalPhone += phone.charAt(i);
				}
			}
			if (finalPhone.length() >= 3) {
				if ("+86".equals(finalPhone.substring(0, 3))) {
					finalPhone = finalPhone.substring(3);
				}
			}
			finalPhone = finalPhone.replaceAll("[+]", "");
			return finalPhone;
		}
}
