package com.quartz.exception;

public class ServiceException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private String code;

	private String message;

	public ServiceException(String message) {
		super();
		this.code = "1000000";
		this.message = message;
	}

	public ServiceException(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public ServiceException(String code, String message, Exception rootException) {
		super(rootException);
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
