/**
* @Title: ReportMailBatch.java
* @Package com.ewp.data.batch
* @Description: 邮件汇报批
* @author zxj
* @date 2018年2月26日
* @version V1.0
*/
package com.quartz.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quartz.service.UserService;

/**
 * @ClassName: ReportMailBatch
 * @Description: 邮件汇报批
 * @author zxj
 * @date 2018年2月26日
 *
 */
@Component
public class UserBatch implements Job {

	private static final Logger log = LoggerFactory.getLogger(UserBatch.class);

	@Autowired
	private UserService service;

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		log.info("ReportMailBatch--data-c->execute()");
		service.say();// 可换成自己业务逻辑
		log.info("--本次邮件汇报批处理结束--");
	}
}
