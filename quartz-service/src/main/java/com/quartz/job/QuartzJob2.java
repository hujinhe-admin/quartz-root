//package com.quartz.job;
//
//import org.quartz.Job;
//import org.quartz.JobExecutionContext;
//import org.quartz.JobExecutionException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import com.quartz.service.UserService;
//
//@Component
//public class QuartzJob2 implements Job {
//	@Autowired
//	UserService userService;
//
//	private void before() {
//		System.out.println("任务2开始执行");
//	}
//
//	@Override
//	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
//		before();
//		System.out.println("开始：" + System.currentTimeMillis());
//		// TODO 业务
//		System.out.println("任务2开始执行");
//		userService.say();
//		System.out.println("Cron.........");
//		System.out.println("结束：" + System.currentTimeMillis());
//		after();
//	}
//
//	private void after() {
//		System.out.println("任务结束执行执行");
//	}
//}
