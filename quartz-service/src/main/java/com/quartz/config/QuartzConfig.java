package com.quartz.config;

import java.io.IOException;
import java.util.Properties;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.quartz.core.JobFactory;

@Configuration
public class QuartzConfig {

	@Autowired
	private JobFactory jobFactory; // 自定义的factory

	@Value("${spring.datasource.url}")
	private String url;// 数据源地址

	@Value("${spring.datasource.username}")
	private String userName;// 用户名

	@Value("${spring.datasource.password}")
	private String password;// 密码

	@Autowired
	QuartzJobProperties quartzProperties;

	@Bean
	public SchedulerFactoryBean schedulerFactoryBean() {
		SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
		try {
			schedulerFactoryBean.setQuartzProperties(quartzProperties());
			schedulerFactoryBean.setJobFactory(jobFactory); // 指向自建的调度工厂，用于解决方法类无法注入的问题
		} catch (IOException e) {
			e.printStackTrace();
		}
		return schedulerFactoryBean;
	}

	@Bean
	public Scheduler scheduler() throws IOException, SchedulerException {
		Scheduler scheduler = schedulerFactoryBean().getScheduler();
		scheduler.start();// 服务启动shi
		return scheduler;
	}

	/**
	 * 
	 * @Title: quartzProperties @Description:
	 *         设置quartz属性 @param @return @param @throws IOException参数 @return
	 *         Properties返回类型 @throws
	 */
	@Bean
	public Properties quartzProperties() throws IOException {
		Properties prop = new Properties();
		prop.put("org.quartz.scheduler.instanceName", quartzProperties.getSchedulerInstanceName());// 调度器的实例名
		prop.put("org.quartz.scheduler.instanceId", quartzProperties.getSchedulerInstanceId());// 实例的标识
		prop.put("org.quartz.scheduler.skipUpdateCheck", quartzProperties.getSchedulerSkipUpdateCheck());// 检查quartz是否有版本更新（true不检查）
		prop.put("org.quartz.jobStore.class", quartzProperties.getJobStoreClass());
		prop.put("org.quartz.jobStore.driverDelegateClass", quartzProperties.getJobStoreDriverDelegateClass());
		prop.put("org.quartz.jobStore.tablePrefix", quartzProperties.getJobStoreTablePrefix());// 表名前缀
		prop.put("org.quartz.jobStore.isClustered", quartzProperties.getJobStoreIsClustered());// 集群开关
		prop.put("org.quartz.threadPool.class", quartzProperties.getThreadPoolClass());// 线程池的名字
		prop.put("org.quartz.threadPool.threadCount", quartzProperties.getThreadPoolThreadCount());// 指定线程数量
		prop.put("org.quartz.threadPool.threadPriority", quartzProperties.getThreadPoolThreadPriority());// 线程优先级（1-10）默认为5
		prop.put("org.quartz.threadPool.threadsInheritContextClassLoaderOfInitializingThread",
				quartzProperties.getThreadPoolThreadsInheritContextClassLoaderOfInitializingThread());
		prop.put("org.quartz.jobStore.dataSource", quartzProperties.getJobStoreDataSource());
		prop.put("org.quartz.dataSource.quartzDataSource.driver",
				quartzProperties.getDataSourceQuartzDataSourceDriver());
		prop.put("org.quartz.dataSource.quartzDataSource.URL", quartzProperties.getDataSourceQuartzDataSourceURL());
		prop.put("org.quartz.dataSource.quartzDataSource.user", quartzProperties.getDataSourceQuartzDataSourceUser());
		prop.put("org.quartz.dataSource.quartzDataSource.password",
				quartzProperties.getDataSourceQuartzDataSourcePassword());
		prop.put("org.quartz.dataSource.quartzDataSource.maxConnections",
				quartzProperties.getDataSourceQuartzDataSourceMaxConnections());
		return prop;
	}

}
