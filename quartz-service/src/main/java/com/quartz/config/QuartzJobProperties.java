package com.quartz.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component  
public class QuartzJobProperties {
	@Value("${org.quartz.scheduler.instanceName}")
	private String schedulerInstanceName;
	@Value("${org.quartz.scheduler.instanceId}")
	private String schedulerInstanceId;
	@Value("${org.quartz.scheduler.skipUpdateCheck}")
	private String schedulerSkipUpdateCheck;
	@Value("${org.quartz.jobStore.class}")
	private String jobStoreClass;
	@Value("${org.quartz.jobStore.driverDelegateClass}")
	private String jobStoreDriverDelegateClass;
	@Value("${org.quartz.jobStore.tablePrefix}")
	private String jobStoreTablePrefix;
	@Value("${org.quartz.jobStore.isClustered}")
	private String jobStoreIsClustered;
	@Value("${org.quartz.threadPool.class}")
	private String threadPoolClass;
	@Value("${org.quartz.threadPool.threadCount}")
	private String threadPoolThreadCount;
	@Value("${org.quartz.threadPool.threadPriority}")
	private String threadPoolThreadPriority;
	@Value("${org.quartz.threadPool.threadsInheritContextClassLoaderOfInitializingThread}")
	private String threadPoolThreadsInheritContextClassLoaderOfInitializingThread;
	@Value("${org.quartz.jobStore.dataSource}")
	private String jobStoreDataSource;
	@Value("${org.quartz.dataSource.quartzDataSource.driver}")
	private String dataSourceQuartzDataSourceDriver;
	@Value("${org.quartz.dataSource.quartzDataSource.URL}")
	private String dataSourceQuartzDataSourceURL;
	@Value("${org.quartz.dataSource.quartzDataSource.user}")
	private String dataSourceQuartzDataSourceUser;
	@Value("${org.quartz.dataSource.quartzDataSource.password}")
	private String dataSourceQuartzDataSourcePassword;
	@Value("${org.quartz.dataSource.quartzDataSource.maxConnections}")
	private String dataSourceQuartzDataSourceMaxConnections;
	public String getSchedulerInstanceName() {
		return schedulerInstanceName;
	}
	public void setSchedulerInstanceName(String schedulerInstanceName) {
		this.schedulerInstanceName = schedulerInstanceName;
	}
	public String getSchedulerInstanceId() {
		return schedulerInstanceId;
	}
	public void setSchedulerInstanceId(String schedulerInstanceId) {
		this.schedulerInstanceId = schedulerInstanceId;
	}
	public String getSchedulerSkipUpdateCheck() {
		return schedulerSkipUpdateCheck;
	}
	public void setSchedulerSkipUpdateCheck(String schedulerSkipUpdateCheck) {
		this.schedulerSkipUpdateCheck = schedulerSkipUpdateCheck;
	}
	public String getJobStoreClass() {
		return jobStoreClass;
	}
	public void setJobStoreClass(String jobStoreClass) {
		this.jobStoreClass = jobStoreClass;
	}
	public String getJobStoreDriverDelegateClass() {
		return jobStoreDriverDelegateClass;
	}
	public void setJobStoreDriverDelegateClass(String jobStoreDriverDelegateClass) {
		this.jobStoreDriverDelegateClass = jobStoreDriverDelegateClass;
	}
	public String getJobStoreTablePrefix() {
		return jobStoreTablePrefix;
	}
	public void setJobStoreTablePrefix(String jobStoreTablePrefix) {
		this.jobStoreTablePrefix = jobStoreTablePrefix;
	}
	public String getJobStoreIsClustered() {
		return jobStoreIsClustered;
	}
	public void setJobStoreIsClustered(String jobStoreIsClustered) {
		this.jobStoreIsClustered = jobStoreIsClustered;
	}
	public String getThreadPoolClass() {
		return threadPoolClass;
	}
	public void setThreadPoolClass(String threadPoolClass) {
		this.threadPoolClass = threadPoolClass;
	}
	public String getThreadPoolThreadCount() {
		return threadPoolThreadCount;
	}
	public void setThreadPoolThreadCount(String threadPoolThreadCount) {
		this.threadPoolThreadCount = threadPoolThreadCount;
	}
	public String getThreadPoolThreadPriority() {
		return threadPoolThreadPriority;
	}
	public void setThreadPoolThreadPriority(String threadPoolThreadPriority) {
		this.threadPoolThreadPriority = threadPoolThreadPriority;
	}
	public String getThreadPoolThreadsInheritContextClassLoaderOfInitializingThread() {
		return threadPoolThreadsInheritContextClassLoaderOfInitializingThread;
	}
	public void setThreadPoolThreadsInheritContextClassLoaderOfInitializingThread(
			String threadPoolThreadsInheritContextClassLoaderOfInitializingThread) {
		this.threadPoolThreadsInheritContextClassLoaderOfInitializingThread = threadPoolThreadsInheritContextClassLoaderOfInitializingThread;
	}
	public String getJobStoreDataSource() {
		return jobStoreDataSource;
	}
	public void setJobStoreDataSource(String jobStoreDataSource) {
		this.jobStoreDataSource = jobStoreDataSource;
	}
	public String getDataSourceQuartzDataSourceDriver() {
		return dataSourceQuartzDataSourceDriver;
	}
	public void setDataSourceQuartzDataSourceDriver(String dataSourceQuartzDataSourceDriver) {
		this.dataSourceQuartzDataSourceDriver = dataSourceQuartzDataSourceDriver;
	}
	public String getDataSourceQuartzDataSourceURL() {
		return dataSourceQuartzDataSourceURL;
	}
	public void setDataSourceQuartzDataSourceURL(String dataSourceQuartzDataSourceURL) {
		this.dataSourceQuartzDataSourceURL = dataSourceQuartzDataSourceURL;
	}
	public String getDataSourceQuartzDataSourceUser() {
		return dataSourceQuartzDataSourceUser;
	}
	public void setDataSourceQuartzDataSourceUser(String dataSourceQuartzDataSourceUser) {
		this.dataSourceQuartzDataSourceUser = dataSourceQuartzDataSourceUser;
	}
	public String getDataSourceQuartzDataSourcePassword() {
		return dataSourceQuartzDataSourcePassword;
	}
	public void setDataSourceQuartzDataSourcePassword(String dataSourceQuartzDataSourcePassword) {
		this.dataSourceQuartzDataSourcePassword = dataSourceQuartzDataSourcePassword;
	}
	public String getDataSourceQuartzDataSourceMaxConnections() {
		return dataSourceQuartzDataSourceMaxConnections;
	}
	public void setDataSourceQuartzDataSourceMaxConnections(String dataSourceQuartzDataSourceMaxConnections) {
		this.dataSourceQuartzDataSourceMaxConnections = dataSourceQuartzDataSourceMaxConnections;
	}
}
