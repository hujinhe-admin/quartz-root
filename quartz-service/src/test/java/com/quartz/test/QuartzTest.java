package com.quartz.test;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.quartz.core.TaskInfo;
import com.quartz.core.TaskService;
import com.quartz.util.DateUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@Configuration
@EnableAutoConfiguration
@SpringApplicationConfiguration(QuartzTest.class)
@ComponentScan(basePackages = { "com.quartz" }) // 可以使用匹配字符
@MapperScan(basePackages = "com.quartz.dao")
@EnableTransactionManagement // 开启注解事务管理
@EnableScheduling
public class QuartzTest {
	@Autowired
	TaskService taskService;

	@Test
	public void quartzTest() {
//		TaskInfo taskInfo = new TaskInfo();
//		taskInfo.setCreateTime(DateUtil.getDateStr(new Date(), "yyyy-MM-dd HH:mm:ss"));
//		taskInfo.setCronExpression("0/1 * * * * ?");
//		taskInfo.setJobDescription("测试job");
//		taskInfo.setJobGroup("UserBatchJobGroup");
//		taskInfo.setJobName("com.quartz.job.UserBatch");
////		taskInfo.setStartDate("2018-10-16 11:32:09");
////		taskInfo.setEndDate("2018-10-18 11:32:09");
//		taskService.addJob(taskInfo);
		taskService.queryJobList();
	}
}
